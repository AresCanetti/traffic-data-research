import xml.etree.ElementTree as Et

class Segments:
    def __init__(self, start, end, parent):
        self.start_node = start
        self.end_node = end
        self.parent = parent

class Elements:
    def __init__(self, element, road_type, info, road_id):
        self.type = road_type
        self.element = element
        self.info = info
        self.id = road_id
        self.name = ""
        self.isRoad = False
        self.nodes = []
        self.intersections = []

    def set_street_name(self, street_name):
        self.name = street_name

    def add_node(self, node):
        self.nodes.append(node)

    def set_intersections(self, int_id):
        self.intersections.append(int_id)





def xml_parsing(file):
    temp = []
    xmldoc = Et.parse(file)
    root = xmldoc.getroot()
    for child in root:
        element = child
        tag = child.tag
        attribs = child.attrib
        info = ["INFO"]
        string = str(attribs)
        parts = string.split("'")
        road_id = parts[3]
        for step_child in child:
            info.append(str(step_child.tag) + " " + str(step_child.attrib))
        new_entry = Elements(element, tag, info, road_id)
        temp.append(new_entry)
    return temp


def get_names(all_elements):
    for elem in all_elements:
        for inf in elem.info:
            if "tag" in inf and "name" in inf and "building" not in inf:
                parts = (inf.split("'v': '", 1))
                if len(parts) > 1:
                    street = parts[1].split("'}", 1)
                    if "tag" not in street[0] and elem.name == "":
                        elem.set_street_name(street[0])


def get_nodes(all_elements):
    # Takes the info lines that contain info about nodes for the element and seperates the line to take only
    # the information we care about(the node number)
    for elem in all_elements:
        for inf in elem.info:
            if "nd" in inf and "ref" in inf:
                parts = inf.split("'")
                elem.add_node(parts[3])


def remove_buildings(road):
    is_building = False
    for inf in road.info:
        if "building" in inf:
            is_building = True
        if "addr" in inf:
            is_building = True
        if "amenity" in inf:
            is_building = True
        if "route" in inf:
            is_building = True
        if "monument" in inf:
            is_building = True
        if "museum" in inf:
            is_building = True
        if "office" in inf:
            is_building = True
        if "tourism" in inf:
            is_building = True
        if "shop" in inf:
            is_building = True
        if "railway" in inf:
            is_building = True
        if "landuse" in inf:
            is_building = True
        if len(road.nodes) == 0:
            is_building = True
    if is_building is False:
        return road
    else:
        return None

# Finds intersections within the list of roads


def find_intersections(roads):
    instersections = []
    for road in roads:
        for node in road.nodes:
            for road1 in roads:
                for node1 in road1.nodes:
                    if node1 == node and road != road1 and node not in instersections and road1.name != road.name:
                        road.set_intersections(node)
                        road1.set_intersections(node)


def find_road_segments(roads, segments):
    for road in roads:
        for x in range(0, len(road.intersections)-1):
            print(road.name)
            print("START NODE " + road.intersections[x])
            print("END NODE " + road.intersections[x+1])
            seg = Segments(road.intersections[x], road.intersections[x+1], road)
            segments.append(seg)




def main():
    # Takes the given OSM data and treats it as an XML file in order to read in data and then creates objects
    # of the element type for the rest of the program to work with
    roads = xml_parsing('HaskellStBig.osm')
    get_names(roads)
    get_nodes(roads)
    new_roads = []
    for road in roads:
        is_road = remove_buildings(road)
        if is_road is not None:
            new_roads.append(is_road)
    find_intersections(new_roads)
    segments = []
    find_road_segments(new_roads, segments)


if __name__ == "__main__":
    main()
