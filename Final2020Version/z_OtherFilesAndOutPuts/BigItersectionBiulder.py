
import csv



#Created by: Michael Jacobs and Ares Canetti
#Licensed under GPL-2.0-or-later



class NodeD:
    def __init__(self, Nid, xlat, ylon, isIntersection ,nType):
        self.id = Nid
        self.isIntersection = isIntersection

        self.type = nType

        self.lat = xlat
        self.lon = ylon


class BigIntersection:
    def __init__(self):
        self.nodesPartOf = []


    def addnode(self, node):
        self.nodesPartOf.append(node)








def V2find_bigIntersections(list, latMax, lonMax, latMin, lonMin):

    with open(('BigIntersection_'+ 'LatMax' + str(latMax)+ 'LonMax' + str(lonMax)+ 'LatMin' + str(latMin)+ 'LonMin' + str(lonMin) + '_' + 'DataDoc.csv'), 'w', newline="") as csvFile:
        writer = csv.writer(csvFile, delimiter=',')

        row = ["Big Intersection ID","Node #", "Node IDs"]
        writer.writerow(row)

        idcount = 0;

        print("At Section 1 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
        arrayHolderList1 = []

        for a in range(0, len(list)):
            for b in range(0, len(list)):
                prin = False
                if(str(list[a].id) != str(list[b].id)):
                    if (((float(list[a].lat) - latMax) < float(list[b].lat)) and (float(list[b].lat) < (float(list[a].lat) + latMax))):
                        if (((float(list[a].lon) - lonMax) < float(list[b].lon)) and (float(list[b].lon) < (float(list[a].lon) + lonMax))):
                            prin = True
                            if (((float(list[a].lat) - latMin) < float(list[b].lat)) and (float(list[b].lat) < (float(list[a].lat) + latMin))):
                                if (((float(list[a].lon) - lonMin) < float(list[b].lon)) and (float(list[b].lon) < (float(list[a].lon) + lonMin))):
                                    prin = False

                if (prin):

                    if (len(arrayHolderList1) == 0):
                        inst = BigIntersection()
                        inst.addnode(list[a])
                        inst.addnode(list[b])
                        arrayHolderList1.append(inst)
                    else:
                        Nconected = True
                        for q in range(0,len(arrayHolderList1)):

                            if list[a] in arrayHolderList1[q].nodesPartOf:
                                if list[b] not in arrayHolderList1[q].nodesPartOf:
                                    arrayHolderList1[q].addnode(list[b])
                                    Nconected = False
                            elif list[b] in arrayHolderList1[q].nodesPartOf:
                                if list[a] not in arrayHolderList1[q].nodesPartOf:
                                    arrayHolderList1[q].addnode(list[a])
                                    Nconected = False
                        if Nconected:
                            inst = BigIntersection()
                            inst.addnode(list[a])
                            inst.addnode(list[b])
                            arrayHolderList1.append(inst)

        print("At Section 2 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")


        '''
        High logic error potential. 
        When removing close nodes in group might remove the node that 
        relates best to a segment in the Big intersection
        '''
        removeItemHolder = []
        for b in range(0, len(arrayHolderList1)):
            removeItemHolder.clear()
            for u in range(0, len(arrayHolderList1[b].nodesPartOf)):
                for g in range(0, len(arrayHolderList1[b].nodesPartOf)):
                    if(arrayHolderList1[b].nodesPartOf[g] != arrayHolderList1[b].nodesPartOf[u] ):
                        if(arrayHolderList1[b].nodesPartOf[u] not in removeItemHolder) and (arrayHolderList1[b].nodesPartOf[g] not in removeItemHolder):
                            if (((float(arrayHolderList1[b].nodesPartOf[u].lat) - latMin) < float(arrayHolderList1[b].nodesPartOf[g].lat)) and (float(arrayHolderList1[b].nodesPartOf[g].lat) < (float(arrayHolderList1[b].nodesPartOf[u].lat) + latMin))):
                                if (((float(arrayHolderList1[b].nodesPartOf[u].lon) - lonMin) < float(arrayHolderList1[b].nodesPartOf[g].lon)) and (float(arrayHolderList1[b].nodesPartOf[g].lon) < (float(arrayHolderList1[b].nodesPartOf[u].lon) + lonMin))):
                                    removeItemHolder.append(arrayHolderList1[b].nodesPartOf[g])



            print()
            for r in range(0, len(removeItemHolder)):
               #print("Note 7: this node is to close to another node: " + str(arrayHolderList1[b].nodesPartOf[r].id)   +   " on loop "  + str(b))
               arrayHolderList1[b].nodesPartOf.remove(removeItemHolder[r])

        print("At Section 3 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")



        notWantedInList = []

        for z in range(0, len(arrayHolderList1)):
            for x in range(0, len(arrayHolderList1)):
                for c in range(0, len(arrayHolderList1[z].nodesPartOf)):
                    if (arrayHolderList1[z] != arrayHolderList1[x]):
                        if(arrayHolderList1[z].nodesPartOf[c] in arrayHolderList1[x].nodesPartOf):
                            print("Note 8: Repete confilct: " + str(arrayHolderList1[z].nodesPartOf[c].id))
                            if(len(arrayHolderList1[z].nodesPartOf) == len( arrayHolderList1[x].nodesPartOf) and (arrayHolderList1[z] not in notWantedInList)):
                                print("Note 9: Reorderd but same: " + str(arrayHolderList1[z].nodesPartOf[c].id))
                                notWantedInList.append(arrayHolderList1[x])
                            elif(len(arrayHolderList1[z].nodesPartOf) < len( arrayHolderList1[x].nodesPartOf)):
                                notWantedInList.append(arrayHolderList1[z])

        print("At Section 4 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")



        bigInterList = []

        for n in range(0, len(arrayHolderList1)):
            if (arrayHolderList1[n] not in notWantedInList):
                if (len(arrayHolderList1[n].nodesPartOf) > 2):
                    inst = BigIntersection()
                    print("--")
                    print("+-" + str(len(arrayHolderList1[n].nodesPartOf)))
                    row = []
                    idcount = idcount + 1
                    row.append(idcount)
                    row.append(len(arrayHolderList1[n].nodesPartOf))
                    for e in range(0, len(arrayHolderList1[n].nodesPartOf)):
                        print("+++",arrayHolderList1[n].nodesPartOf[e].id)
                        inst.addnode(arrayHolderList1[n].nodesPartOf[e])
                        row.append(arrayHolderList1[n].nodesPartOf[e].id)
                    bigInterList.append(inst)
                    writer.writerow(row)

        print("At Section 5 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")




        return bigInterList











def main():
    print("Starting Big item Script")
    NodeDataFile = 'TrafficImageNodeDataPlaceFolder/NodeInfo_DataDoc.csv'
    MaxLat = .00020
    MaxLon = .00020
    MinLat = .00004
    MinLon = .00004
    print("Found File " + str(NodeDataFile))




    nodeList = []

    with open(NodeDataFile) as csvFile:
        reader = csv.reader(csvFile, delimiter=',')
        for row in reader:
            if (row[0] != "Node ID"):
                newNd = NodeD(row[0],row[1],row[2],row[3],row[4])
                nodeList.append(newNd)
            # print("Id Added ", row[0])


    V2find_bigIntersections(nodeList, MaxLat, MaxLon, MinLat, MinLon)






if __name__ == "__main__":
    main()
