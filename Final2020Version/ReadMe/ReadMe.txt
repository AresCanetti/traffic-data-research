﻿Getting Started
This code is made up of two basic parts: the Te_OSM_Worker.py script which takes in a special file type based on an open source mapping service called open street maps. The Te_OSM_Worker takes in a file and finds all the intersections and other important features and outputs all relevant data to files. Then, the TrafficImageSystem.py file takes in the files created by the OSMParser and uses the coordinates parsed out in the Te_OSM_Worker and uses google maps to find a color and assigns a numeric value to it.
====================================================================================================================================== 
Running on a new city 
================================= 
In order to run on a new city you need the osm file for that city. Through our experiences this can quickly be found by googling the city name and osm and usually is kept on a government website or you can use www.openstreetmap.org to export an area into an osm file (you might need api if its a vary big are)
============================================= 
Working with Te_OSM_Worker.py 
================================= 
This file should be found in the main folder.
This script takes in an OSM file and outputs csv files holding Node, Segment, 2 way connection, and 3 way connection data.


        Inputting OSM maps:
From the main folder navigate to the “OSMFilePlaceFolder” folder. place all OSM files youd like to work with in this folder. When the code runs it will work on all the OSM files in the folder one after another.


                or


In def main at the top you will see variables which will probably be what you want to change. Variable name “file_name” set this to the osm you'd like to use (ex. 'LilAustin.osm') to use un comment the variable and comment out the folder reader. Though we recommend the first method. 


        Outputs:
You can find the outputs in the “Te_OutPuts” folder with each csv type found in its respective folder. 
Something to note is that the csv_Nodes appart from outputting to its individual folder based on its location also adds its nodes to the “NodeInfo_DataDoc.csv” file used in “TrafficImageSystem.py”.
If you would like to use one of the other file output types whose functions are still in “Te_OSM_Worker.py” you can uncomment them and find their folders in the “z_OtherFilesAndOutPuts” along side the original “te.py” and “BigItersectionBiulder.py”.


        Other important notes: 
- If you don't want to print to a csv for one of the file types then at the bottom of the main comment out the functions [csv_Segment, csv_Segment2, csv_Segment3, or csv_Nodes]
- There is a function called csv_Intersections0. This is needed to get Node types for the csv_Nodes files. The csv_Intersections0 does not output any file itself. 
- If your looking at the console prints and the code seems to freeze after printing "g loading" its not frozen it may take a while (hours depending on the size of OSM map) .
- if you seem to be picking up false positives for roads "such as rivers, train tracks, parktrails, or buildings" look at that ways pull info (ex. , "tag {'k': 'cycleway:right', 'v': 'shared'}", "tag {'k': 'highway', 'v': 'primary'}", ...) then find the key work for what it is (ex. building, railway, waterway) and add it to the function "remove_buildings" as part of the elif chain. This will remove it from your road list. 
- if you find intersections that don't seem to be complete (ex. four way intersection only having three segments attached or missing a node) check to see if its not at the border/ outside your osm map. Do this by searching the console print for references to it as a start or end node if its not there it is probably outside the osm map. 
                - Use www.openstreetmap.org to export an area into an osm file (you might need api if its a very big area) 
- Use www.openstreetmap.org to view nodes and ways on a map. :to view a node the url should be www.openstreetmap.org/node/"nodeId" (ex. www.openstreetmap.org/node/4345054664) :to view a way the url should be www.openstreetmap.org/way/"wayId" (ex. www.openstreetmap.org/way/436625345)


============================================= 
Working with TrafficImageSystem.py 
================================= 
This file should be found in the main folder.
This script generates traffic data based on nodes by screenshotting google maps at that nodes lat lon and saving the road color. 


        Input Node date:
“TrafficImageSystem.py“ works off of the csv file “NodeInfo_DataDoc.csv” which is the file containing all node data. It can be found in the “TrafficImageNodeDataPlaceFolder” folder (note that the name of this file is unique and putting other things in this folder will not be read)


Multi Runs:
Near the top of the script there is a variable “entireSetRepet” if you want to repeat the checking of data multiple times set this to # past 1. use this if you are collecting data on the same set for an extended time. (also note that during the run chrome windows will be opening and closing and it is difficult to use the computer during this time and is recommended this be left to work) (also note that the time to nodes worked on is about 1000 nodes per hour.


        Outputs:
There is a csv named “-OutPut_TrafficInfoNodeInfo_DataDoc_TrafficDataDoc.csv” all traffic data is appended to this file.


        Other important notes: 
- there is a folder named “z_ArchiveDATA_Backups” with a few backups of the “-OutPut_TrafficInfoNodeInfo_DataDoc_TrafficDataDoc.csv” data from different points in time. This is not automatically added to and must be manually copied into the folder.
- there is a png named “my_screenshot.png” this is needed by “TrafficImageSystem.py“ when capturing and reading the google maps data and will contain the last screenshot the script took (note that screen shot is only for the chrome window opened by the script)
- You may need to import pip installs for all this to run
- commented under the first writer call is a commented line that sets the first row of the csv with labels. Uncomment if making a new file and is first run then comment out.         
- the folder “webdrivers” there is a driver named chromedriver. This is needed to open the chrome windows. If chrome window doesn't open you may need to install a web drivers\chromedriver at http://chromedriver.chromium.org/downloads , this may also be needed if you're chrome version updates currently driver works on version 79 of chrome.
- if you are only getting blank streets on ALL nodes (not just residential or low traffic areas). Go to google maps in another chromeWindow and turn on traffic view which will show a colored line of traffic.
- also if when checking the data you see that clearly colored streets are not being recorded you may need to add its color code to the list of accepted colors in the chain of if elses what determines how the traffic level gets tagged.
- there is a variable named “fileprint” located near the top of the script. if set to true will print to file. set to false when testing as writing to file will append and its easy to destroy a dataset by mistake. 
- when the chrome window is opening and closing if it sees a white window(reads a white pixel) it will close that window to reopen chrome. This is to avoid chrome asking if the script is a robot.
- the script comes with a timer that if a chrome window is open too long it will force scip that node and move on. This is to avoid freezing on a window or loading errors.