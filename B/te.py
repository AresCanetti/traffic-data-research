import xml.etree.ElementTree as ET
import csv
import json
import glob

# Created by: Michael Jacobs and Ares Canetti
# Licensed under GPL-2.0-or-later
'''===================================================================================================================== class Segments
Segments Class part of larger default open street map segment
'''


class Segments:
    def __init__(self, start, end, parent):
        self.start_node = start
        self.end_node = end
        self.parent = parent
        self.segment_id = (str(self.parent.id) + "_" + str(len(self.parent.segments) + 1))
        self.parent.add_seg(self)

    def printInfo(self, roadInfoList):

        print("Segment Id :" + str(self.segment_id))
        print("StartNode  :" + str(self.start_node.id))
        print("EndNode    :" + str(self.end_node.id))
        print("Parent Id  :" + str(self.parent.id))

        #for i in range(0, len(roadInfoList)):
            #if roadInfoList[i].id == self.parent.id:
                #roadInfoList[i].printAll()

        #print("#######################################################################################################")


'''===================================================================================================================== class Elements
Elements are the ways form open street map
'''


class Elements:
    def __init__(self, element, road_type, info, road_id):
        self.type = road_type
        self.element = element
        self.info = info
        self.id = road_id
        self.name = ""
        self.isRoad = False
        self.nodes = []
        self.intersections = []
        self.numintersect = 0
        self.segments = []

    def set_street_name(self, street_name):
        self.name = street_name

    def add_node(self, node):
        self.nodes.append(node)

    def set_intersections(self, int_id):
        self.intersections.append(int_id)

    def set_numintersect(self, num):
        self.numintersect = num

    def add_seg(self, seg):
        self.segments.append(seg)


'''===================================================================================================================== class RoadElementsv
RoadElements is where the information for roads is held (such as #of lanes or streat condition
'''


class RoadElements:
    def __init__(self, info, road_id, name, place, highway, lanes, turnlane, oneway, surface, lit, sidewalk, leftSP,
                 rightSP, bothSP, direction):
        self.info = info
        self.id = road_id
        self.name = name
        self.place = place
        self.highway = highway
        self.lanes = lanes
        self.turnLane = turnlane
        self.oneWay = oneway
        self.surface = surface
        self.lit = lit
        self.sideWalk = sidewalk
        self.leftSP = leftSP
        self.rightSP = rightSP
        self.bothSP = bothSP
        self.direction = direction

    def printAll(self):
        print("-------------------------------------------------------------------------------------------------------")
        print(self.info)
        print("Name-----|", self.name)
        print("Place----|", self.place)
        print("HighWay--|", self.highway)
        print("Lanes----|", self.lanes)
        print("TurnLanes|", self.turnLane)
        print("OneWay---|", self.oneWay)
        print("Surface--|", self.surface)
        print("Lit------|", self.lit)
        print("SideWalk-|", self.sideWalk)
        print("LeftSP---|", self.leftSP)
        print("RightSP--|", self.rightSP)
        print("BothSP---|", self.bothSP)
        print("Direction|", self.direction)
        print("-------------------------------------------------------------------------------------------------------")


'''===================================================================================================================== class NodeD
Simple Node Object with Lat and Lon
'''


class NodeD:
    def __init__(self, Nid, xlat, ylon, nType):
        self.id = Nid
        self.isIntersection = False

        # 0 is nothing
        # 1 is light
        # 2 is crossing
        # 3 is stop
        # 4 is laneChange

        self.type = nType

        self.lat = xlat
        self.lon = ylon


'''===================================================================================================================== xml_parsing
Parse File to get all ways
'''


def xml_parsing(file):
    temp = []
    nodeList = []
    xmldoc = ET.parse(file)
    root = xmldoc.getroot()
    for child in root:
        element = child
        tag = child.tag
        attribs = child.attrib
        info = []
        string = str(attribs)
        parts = string.split("'")
        road_id = parts[3]
        for step_child in child:
            info.append(str(step_child.tag) + " " + str(step_child.attrib))
        new_entry = Elements(element, tag, info, road_id)
        temp.append(new_entry)
        if child.tag == "node":
            type = 0
            if len(str(child.attrib).split(', ')) > 7:
                if "'lat':" in (str(child.attrib).split(', ')[7]):
                    # print('Node lat', str(str(child.attrib).split(', ')[7]).split()[1])
                    nlat = str(str(child.attrib).split(', ')[7]).split()[1].replace("'", '').replace("}", '').replace(
                        " ", '')
                if len(str(child.attrib).split(', ')) > 8:
                    if "'lon':" in (str(child.attrib).split(', ')[8]):
                        # print('Node lon', str(str(child.attrib).split(', ')[8]).split()[1])
                        nlon = str(str(child.attrib).split(', ')[8]).split()[1].replace("'", '').replace("}",
                                                                                                         '').replace(
                            " ", '')
            #print("NODE " + road_id + nlat + nlon)
            for inf in info:
                if "highway" in inf and "stop" in inf:
                    type = 3
                if "highway" in inf and "crossing" in inf:
                    type = 2
                if "highway" in inf and "traffic_signals" in inf:
                    type = 1
            newNode = NodeD(road_id, nlat, nlon, type)
            nodeList.append(newNode)
    return temp, nodeList


'''===================================================================================================================== get_names
Get way name
'''


def get_names(all_elements):
    for elem in all_elements:
        for inf in elem.info:
            if "tag" in inf and "name" in inf and "building" not in inf:
                parts = (inf.split("'v': '", 1))
                if len(parts) > 1:
                    street = parts[1].split("'}", 1)
                    if "tag" not in street[0] and elem.name == "":
                        elem.set_street_name(street[0])


'''===================================================================================================================== get_nodes
Seperate node form way
'''


def get_nodes(all_Elements):
    for elem in all_Elements:
        for inf in elem.info:
            if "nd" in inf and "ref" in inf:
                parts = inf.split("'")
                elem.add_node(parts[3])


'''===================================================================================================================== remove_buildings
Removes non road objects from Way list
'''


def remove_buildings(road):
    is_building = False
    for inf in road.info:
        if "building" in inf:
            is_building = True
        elif "addr" in inf:
            is_building = True
        elif "amenity" in inf:
            is_building = True
        elif "route" in inf:
            is_building = True
        elif "monument" in inf:
            is_building = True
        elif "museum" in inf:
            is_building = True
        elif "office" in inf:
            is_building = True
        elif "tourism" in inf:
            is_building = True
        elif "shop" in inf:
            is_building = True
        elif "railway" in inf:
            is_building = True
        elif "landuse" in inf:
            is_building = True
        elif "waterway" in inf:
            is_building = True
        elif len(road.nodes) == 0:
            is_building = True
    if is_building is False:
        return road
    else:
        return None


'''===================================================================================================================== find_intersections
Findes nodes that overlap and label them as intersections
'''


def find_intersections(roads, nodeList):
    instersections = []
    for road in roads:
        for road1 in roads:
            for node in road.nodes:
                for node1 in road1.nodes:
                    # and road != road1) and node not in instersections
                    if (node1 == node and road != road1):
                        road.set_numintersect(road.numintersect + 1)

                        # instersections.append(node)
                        road.set_intersections(str(node))

                        # road1.set_intersections(str(node+"        _"+ road.name + "&" +"_"+ road1.name ))
                        for n in range(0, len(nodeList)):
                            if (str(nodeList[n].id) == node):
                                nodeList[n].isIntersection = True
                                if (nodeList[n] not in instersections):
                                    instersections.append(nodeList[n])
    return instersections


'''===================================================================================================================== find_road_segments
Splits road into segments with only two nodes and a parent
'''


def find_road_segments(roads, nodeList):
    temp = []
    Stnode = None
    Ennode = None
    for road in roads:
        if (road.name != ""):
            #print("##########################==============================================------------------------------------------------")
            #print("Nodes------:" + str(road.nodes))
            for x in range(0, (len(road.nodes) - 1)):
                if (str(road.nodes[x]) != str(road.nodes[x + 1])):
                    #print("Name-------:" + road.name)
                    # print("Num-------:" + str(road.numintersect-1) + "__" +str(x))
                    #print("START NODE-:" + road.nodes[x])
                    #print("END NODE---:" + road.nodes[x + 1])

                    for i in range(0, len(nodeList)):
                        if nodeList[i].id == str(road.nodes[x]):
                            Stnode = nodeList[i]
                    for i in range(0, len(nodeList)):
                        if nodeList[i].id == str(road.nodes[x + 1]):
                            Ennode = nodeList[i]

                    new_entry = Segments(Stnode, Ennode, road)
                    # new_entry.SegmentId = str(x) + "_" + str(road.id)
                    temp.append(new_entry)
    return temp


'''===================================================================================================================== print_Segments
Print all sugments and there information
'''


def print_Segments(seg, listInfo):
    for s in seg:
        s.printInfo(listInfo)


'''===================================================================================================================== getroadInfo
Parse way info and adds it to the roadElement Class
'''


def getroadInfo(road):
    #print(road.info)

    info = road.info
    name = '. '
    place = '. '
    highway = '. '
    lanes = '. '
    turnlane = '. '
    oneway = '. '
    surface = '. '
    lit = '. '
    sidewalk = '. '
    leftSP = '. '
    rightSP = '. '
    bothSP = '. '
    direct = '. '

    for i in range(0, len(str(road.info).split(', '))):
        if "'k': 'oneway'" in str(road.info).split(', ')[i]:
            #print("Oneway:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #         ']', ''))
            oneway = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                  '').replace(
                ']', '')
        elif "'k': 'highway'" in str(road.info).split(', ')[i]:
            #print("Highway:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #         ']', ''))
            highway = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                   '').replace(
                ']', '')
        elif "turn:lanes" in str(road.info).split(', ')[i]:
            #print("Turn Lanes:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          "]", ''))
            turnlane = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                    '').replace(
                "]", '')
        elif "'k': 'lanes'" in str(road.info).split(', ')[i]:
            #print("Lanes:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            lanes = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                 '').replace(
                ']', '')
        elif "'k': 'name'" in str(road.info).split(', ')[i]:
            #print("Name:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            name = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                '').replace(
                ']', '')
        elif "'k': 'tiger:county'" in str(road.info).split(', ')[i]:
            #print("Place:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"', ''),
            #      ",", (str(road.info).split(', ')[i + 2]).replace("'", '').replace("}", '').replace('"', ''))
            place = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                 '') + "," + (
                    str(road.info).split(', ')[i + 2]).replace("'", '').replace("}", '').replace('"', '')
        elif "'k': 'sidewalk'" in str(road.info).split(', ')[i]:
            #print("Side Walk:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            sidewalk = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                    '').replace(
                ']', '')
        elif "'k': 'parking:lane:left'" in str(road.info).split(', ')[i]:
            #print("Left Side Parking:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            leftSP = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                  '').replace(
                ']', '')
        elif "'k': 'parking:lane:right'" in str(road.info).split(', ')[i]:
            #print("Right Side Parking:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            rightSP = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                   '').replace(
                ']', '')
        elif "'k': 'parking:lane:both'" in str(road.info).split(', ')[i]:
            #print("Both Side Parking:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            bothSP = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                  '').replace(
                ']', '')
        elif "'k': 'lit'" in str(road.info).split(', ')[i]:
            #print("Lit:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            lit = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                               '').replace(
                ']', '')
        elif "'k': 'surface'" in str(road.info).split(', ')[i]:
            #print("Surface:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            surface = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                   '').replace(
                ']', '')
        elif "'k': 'tiger:name_direction_prefix'" in str(road.info).split(', ')[i]:
            #print("Direction:",
            #      (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
            #                                                                                                   '').replace(
            #          ']', ''))
            direct = (str(road.info).split(', ')[i + 1]).split(': ')[1].replace("'", '').replace("}", '').replace('"',
                                                                                                                  '').replace(
                ']', '')

    new_entry = RoadElements(info, road.id, name, place, highway, lanes, turnlane, oneway, surface, lit, sidewalk,
                             leftSP, rightSP, bothSP, direct)
    #new_entry.printAll()
    return new_entry


def lane_NodeCheck(nodeList, SegmentList, roadInfoList):
    print("g Loading")
    for n in range(0, len(nodeList)):
        for o in range(0, len(SegmentList)):
            for t in range(0, len(SegmentList)):
                if (SegmentList[o].parent.name == SegmentList[t].parent.name):
                    if (((SegmentList[o].start_node == nodeList[n]) or (SegmentList[o].end_node == nodeList[n])) and (
                        (SegmentList[t].start_node == nodeList[n]) or (SegmentList[t].end_node == nodeList[n]))):
                        for e in range(0, len(roadInfoList)):
                            for s in range(0, len(roadInfoList)):
                                if ((roadInfoList[e].id == SegmentList[o].parent.id) and (
                                    roadInfoList[s].id == SegmentList[t].parent.id)):
                                    if (roadInfoList[e].lanes != roadInfoList[s].lanes):
                                        if (nodeList[n].type == 0):
                                            nodeList[n].type = 4
                                            #print("Node Lane Found: ", nodeList[n].id)
    print("o Done Loading")


def write_to_json(road_segments, roads, file_name):
    data = {}
    data['road_segments'] = []
    data['parent_roads'] = []

    for seg in road_segments:
        data['road_segments'].append({
            'id': seg.segment_id,
            'start_node_id': seg.start_node.id,
            'start_node_lat': seg.start_node.lat,
            'start_node_long': seg.start_node.lon,
            'end_node_id': seg.end_node.id,
            'end_node_lat': seg.end_node.lat,
            'end_node_long': seg.end_node.lon,
        })
    for road in roads:
        data['parent_roads'].append({
            'id': road.id,
            'name': road.name,
            'place': road.place,
            'highway': road.highway,
            'lanes': road.lanes,
            'turnLane': road.turnLane,
            'oneWay': road.oneWay,
            'surface': road.surface,
            'lit': road.lit,
            'sideWalk': road.sideWalk,
            'leftSP': road.leftSP,
            'rightSP': road.rightSP,
            'bothSP': road.bothSP

        })
    parts = file_name.split()
    json_file_name = (parts[0] + ".json")
    with open(json_file_name, 'w') as outfile:
        json.dump(data, outfile)


def csv_Nodes(nodeList, file_name):
    parts = file_name.split(".")
    parts[0] = str(parts[0].split("OSMFilePlaceFolder/"))
    csv_file = "/Users/Ares/PycharmProjects/A/OutPut_Node_Data/" + parts[0] + "_Node_Data.csv"
    with open(csv_file, 'w', newline="") as csvFile:
        writer = csv.writer(csvFile, delimiter=',')

        row = ["Node ID", "Lat", "Lon", "Is Intersection", "Type"]
        writer.writerow(row)

        for k in range(0, len(nodeList)):
            row = [nodeList[k].id, nodeList[k].lat, nodeList[k].lon, nodeList[k].isIntersection, nodeList[k].type]
            writer.writerow(row)


def csv_Segment(segList, roadInfoList, file_name):
    parts = file_name.split(".")
    parts[0] = str(parts[0].split("OSMFilePlaceFolder/"))
    seg_file = "/Users/Ares/PycharmProjects/A/OutPut_Seg_Data/" + parts[0] + "_Seg_Data.csv"

    with open(seg_file, 'w', newline="") as csvFile:
        writer = csv.writer(csvFile, delimiter=',')

        row = ["Segment Id", "Start Node", "End Node"
            , "Perant Road ID"
            , "Road Name"
            , "State"
            , "Road Type"
            , "Lanes"
            , "TurnLanes"
            , "OneWay"
            , "Surface"
            , "Lit"
            , "SideWalk"
            , "Left Side parking"
            , "Right Side parking"
            , "Both Side parking"
            , "Direction"]
        writer.writerow(row)

        for k in range(0, len(segList)):
            for i in range(0, len(roadInfoList)):
                if (segList[k].parent.id == roadInfoList[i].id):
                    row = [segList[k].segment_id, segList[k].start_node.id, segList[k].end_node.id
                        , roadInfoList[i].id
                        , roadInfoList[i].name
                        , roadInfoList[i].place
                        , roadInfoList[i].highway
                        , roadInfoList[i].lanes
                        , roadInfoList[i].turnLane
                        , roadInfoList[i].oneWay
                        , roadInfoList[i].surface
                        , roadInfoList[i].lit
                        , roadInfoList[i].sideWalk
                        , roadInfoList[i].leftSP
                        , roadInfoList[i].rightSP
                        , roadInfoList[i].bothSP
                        , roadInfoList[i].direction]

                    writer.writerow(row)
                    i = len(roadInfoList)


def csv_Intersections(lilList, segList, file_name):
    parts = file_name.split(".")
    parts[0] = str(parts[0].split("OSMFilePlaceFolder/"))
    int_file = "/Users/Ares/PycharmProjects/A/OutPut_Intersections/" + parts[0] + "_Intersections.csv"

    with open(int_file, 'w', newline="") as csvFile:
        writer = csv.writer(csvFile, delimiter=',')

        row = ["Node Type", "Segment IDs"]
        writer.writerow(row)

        for l in range(0, len(lilList)):
            if (lilList[l].isIntersection):
                inIntersec = []
                #print("***")
                inIntersec.append(lilList[l].type)
                for m in range(0, len(segList)):
                    if ((segList[m].start_node == lilList[l]) or (segList[m].end_node == lilList[l])):
                        #print(segList[m].segment_id)
                        inIntersec.append(segList[m].segment_id)
                row = inIntersec
                writer.writerow(row)


'''===================================================================================================================== Main
Main
'''


def main():
    path = 'OSMFilePlaceFolder'
    files = [f for f in glob.glob(path + "**/*.osm", recursive=True)]
    print(files)
    for f in files:
        print("Read in Folder: ", f.split("/")[1])
    for f in files:
        print("Now On: ", f.split("/")[1])
        file_name = "OSMFilePlaceFolder/" + f.split("/")[1]

        # file_name = 'LilAustin.osm'








        roads, nodeList = xml_parsing(file_name)

        get_names(roads)
        get_nodes(roads)

        new_roads = []

        nodeSet = []
        nodeSeen = []

        for road in roads:
            is_road = remove_buildings(road)
            if is_road != None:
                new_roads.append(is_road)
        for road in new_roads:
            if road.name != "":
                #print(road.name)
                for nd in road.nodes:
                    #print("_", nd)
                    nodeSet.append(nd);

        l_roadInfo = []
        for road in new_roads:
            if road.name != "":
                l_roadInfo.append(getroadInfo(road))
                #print('-')

        segmentList = find_road_segments(new_roads, nodeList)
        lane_NodeCheck(nodeList, segmentList, l_roadInfo)
        #print_Segments(segmentList, l_roadInfo)
        write_to_json(segmentList, l_roadInfo, file_name)

        roadNodes = []
        for k in range(0, len(nodeList)):
            for i in range(0, len(nodeSet)):
                if str(nodeSet[i]) in nodeList[k].id:
                    if str(nodeSet[i]) not in nodeSeen:
                        # print(k, '---id', nodeList[k].id, '---lat', nodeList[k].lat, '---lon', nodeList[k].lon)
                        nodeSeen.append(nodeSet[i])
                        roadNodes.append(nodeList[k])

        find_intersections(new_roads, nodeList)

        csv_Nodes(roadNodes, file_name)
        csv_Segment(segmentList, l_roadInfo, file_name)
        csv_Intersections(roadNodes, segmentList, file_name)


if __name__ == "__main__":
    main()
