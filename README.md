Getting Started
This code is made up of three basic parts the OSMParser which takes in a special file type based on an open source mapping service called open street maps. The OSMParser takes in a file and finds all the intersections and other important features and outputs all relevant data to files. Then, the TrafficImageSystem_v2 takes in the files created by the OSMParser and uses the coordinates parsed out in the OSMParser and uses google maps to find a color and assigns a numeric value to it and that is stored and can be used later. Finally, there is a big intersection builder that takes the nodes near each other and groups them if they belong to a bigger intersection. ====================================================================================================================================== 
Running on a new city 
================================= 
In order to run on a new city you need the osm file for that city. Through our experiences this can quickly be found by googling the city name and osm and usually is kept on a government website or you can use www.openstreetmap.orgto export an area into an osm file (you might need api if its a vary big are). Once you find the file you need to put it in the OSMParser folder. Then, you must go into the code and in the main there is a variable called file_name rename that whatever the new downloaded file is called. Once that is done you should be left with csv files that are named according to what information they hold these are important because they are used later in the traffic portion. The OSMPArser can take awhile to run because it is working with thousands of nodes so, just be patient you may want to let it run overnight. 

============================================= 
Working with OSMParser_GetInfo_V0.3.py 
================================= 
This script takes in an osm file and outputs csv files holding node, segment, and intersection data 
It is located "traffic-data-research\OSMParser_GetInfo_V0.3"

	In the OSMParser_GetInfo_V0.3 folder there is a folder "OSMFilePlaceFolder" place all osm files youd like to work with in this folder. when the code runs it will work on all the osm files in the folder one after another

		or

	In main at the top you will see variables which will probably be what you want to change 
		-file_name: set this to the osm you'd like to use (ex. 'LilAustin.osm') to use un comment the variable and comment out the folder reader

	other notes: 
		- if you don't want to print to a csv for one of the file types then at the bottom of the main comment out csv_Nodes csv_Segment csv_Intersections 
		- if your looking at the console prints and the code seems to freeze after printing "g" its not frozen it may take a while (an hour) depending on the node and segment count. it will then print "o" 
		- if you seem to be picking up false positives for roads "such as rivers, train tracks, parktrails, or buildings" look at that ways pull info (ex. , "tag {'k': 'cycleway:right', 'v': 'shared'}", "tag {'k': 'highway', 'v': 'primary'}", ...) then find the key work for what it is (ex. building, railway, waterway) and add it to the function "remove_buildings" as part of the elif chain. This will remove it from your road list 
		- if you find intersections that don't seem to be complete (ex. four way intersection only having three segments attached or missing a node) check to see if its not at the border/ outside your osm map. Do this by searching the console print for references to it as a start or end node if its not there it is probably outside the osm map. 
		- you can use www.openstreetmap.org to export an area into an osm file (you might need api if its a vary big are) 
		- you can use www.openstreetmap.org to view nodes and ways on a map. :to view a node the url should be www.openstreetmap.org/node/"nodeId" (ex. www.openstreetmap.org/node/4345054664) :to view a way the url should be www.openstreetmap.org/way/"wayId" (ex. www.openstreetmap.org/way/436625345)

============================================= 
Working with TrafficImageSystem_V2.py 
================================= 
This script is responsible for generating the traffic data based on nodes by screenshotting google maps at that nodes lat lon and saving the road color. 
It is located "traffic-data-research\TrafficDataScripts"

	In the TrafficDataScripts folder there is a folder "TrafficImageNodeDataPlaceFolder" place all csv NodeData files youd like to work with in this folder. when the code runs it will work on all the csv files in the folder one after another
--"NOTE DATA MIGHT NOT WRITE IF STOPPED BEFORE FINISHED SCRIPT (1000 nodes = 1 hour) TAKE THIS INTO ACCOUNT WHEN CHANGING THIS"-- 

		or

	In main at the top you will see four variable which will probably be what you want to change 
		-fileprint: if set to true will print to file. set to false when testing as writing to file will append and its easy to destroy a dataset by mistake. 
		-entireSetRepet: if you want to repeat the checking of data multiple times set this to # past 1. use this if you are collection data on the same set for an extended time. 		--"NOTE DATA MIGHT NOT WRITE IF STOPPED BEFORE FINISHED SCRIPT (1000 nodes = 1 hour) TAKE THIS INTO ACCOUNT WHEN CHANGING THIS"-- 
		-trafficDataFile: this should be set to the name of the traffic file you'd like to create or/and append too (ex. 'TrafficDataDoc.csv') 
		-NodeDataFile: this should be set to the name of the node data file you'd like to get traffic from (ex. 'NodeInfo_DataDoc.csv') The code will open a chrome browser multiple times in the run to take screenshots. Do not close these yourself it will crash the code.

	other notes: 
		- You may need to import pip installs for this to run
		-commented under the first writer call is a commented line that sets the first row of the csv with labels. Uncomment if making a new file and is first run then comment out. 		-if chrome window doesn't open you may need to install a web drivers\chromedriver at http://chromedriver.chromium.org/downloads
		-if you are only getting blank streets on ALL nodes (not just residential or low traffic areas). Go to google maps in another chromeWindow and turn on traffic view which will show a colored line of traffic.

============================================= 
Working with BigItersectionBiulder.py 
================================= 
This script makes larger intersections by seeing how close eachNode is to one another. 
It is located "traffic-data-research\OSMParser_GetInfo_V0.3"
	In main at the top you will see variables which will probably be what you want to change 
		-NodeDataFile: this should be set to the name of the node data file you'd like to work with(ex. 'NodeInfo_DataDoc.csv'). 
		-MaxLat and Max Lon: this determines the max distance nodes can be for them to be considered part of a group "Note this value is a decimal based on coordinates as such something like .0002 is a larger distance. 
		-MinLat and Min Lon: this determines the min distance nodes must be for them to be considered part of a group this is to prevent nodes that are on top or unusually close from being added into a group 
		--"NOTE nodes added to a group will chain to a node out of reach of one node might be in reach of another node in the group and as such will be added. Take this into account"--

	other notes: 
		-the data file generated will be named "BigIntersection_, LatMax, LonMax, LatMin, LonMin, _DataDoc.csv" 
		-WRITE CODE: if you'd like to make this code work with segments just extract all the start and end nodes into one list and run with that instead of csv file.

