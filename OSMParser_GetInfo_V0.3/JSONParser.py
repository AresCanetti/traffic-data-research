import json


def main():
    with open('roadAndSegData.json', 'r') as file:
        street_data = json.load(file)
    for data in street_data['road_segments']:
        segment_id = data['id']
        start_node_id = data['start_node_id']
        start_node_lat = data['start_node_lat']
        start_node_long = data['start_node_long']
        end_node_id = data['end_node_id']
        end_node_lat = data['end_node_lat']
        end_node_long = data['end_node_long']
    for data in street_data['parent_roads']:
        road_id = data['id']
        road_name = data['name']
        road_place = data['place']
        road_highway = data['highway']
        num_lanes = data['lanes']
        has_turnLane = data['turnLane']
        is_oneWay = data['oneWay']
        surface_type = data['surface']
        is_lit = data['lit']
        has_sidewalk = data['sideWalk']
        l_sp = data['leftSP']
        r_sp = data['rightSP']
        b_sp = data['bothSP']


if __name__ == "__main__":
    main()
