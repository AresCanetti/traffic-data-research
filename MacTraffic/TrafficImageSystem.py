from PIL import Image
import urllib.request
import time
import datetime
import calendar;
from datetime import datetime
from selenium import webdriver
import csv
import sys
import random
import glob
import time

# Created by: Michael Jacobs and Ares Canetti
# Licensed under GPL-2.0-or-later

def main():  # {
    print("in main")

    path = 'TrafficImageNodeDataPlaceFolder'
    files = [f for f in glob.glob(path + "**/*.csv", recursive=True)]
    for f in files:
        print("Read in Folder: ", f.split("/")[1])
    for f in files:
        print("Now On: ", f.split("/")[1])
        NodeDataFile = f.split("/")[1]

        filprint = True  # if false will not print into file
        entireSetRepet = 10  # run the set # of times

        # NodeDataFile = 'NodeInfo_DataDoc.csv'
        trafficDataFile = NodeDataFile.replace(".csv", '') + '_TrafficDataDoc.csv'

        print("trafficDataFile: "+trafficDataFile)

        with open("-OutPut_TrafficInfo"+trafficDataFile, 'a', newline="") as csvFile:
            writer = csv.writer(csvFile, delimiter=',')
            # writer.writerow(['Traffic Id','Node Id', 'xLat', 'yLon', 'Color (0-White 1-Green 2-Orange 3-Red 4-DarkRed)', 'PixleCor', 'Time', 'Day (0-6 Monday-Sunday)'])





            driver = webdriver.Chrome('/Users/Ares/PycharmProjects/A/webdrivers/chromedriver')
            print(driver)

            trafficId = 0

            with open(trafficDataFile) as csvFile3:
                reader = csv.reader(csvFile3, delimiter=',')
                for row in reader:
                    # print("Id Added ", row[0])
                    trafficId = (row[0])
                print("lastTrafficId ", trafficId)
                if (trafficId == 'Traffic Id'):
                    trafficId = 0
                else:
                    trafficId = int(trafficId)
                    trafficId = int(trafficId + 1)

            idArray = []
            xArrayLat = []
            yArrayLon = []
            print(NodeDataFile)
            with open("/Users/Ares/PycharmProjects/A/TrafficImageNodeDataPlaceFolder/" + NodeDataFile) as csvFile2:
                readCSV = csv.reader(csvFile2, delimiter=',')
                for row in readCSV:
                    if (str(row[0]) != "Node ID"):
                        idArray.append(str(row[0]).replace("'", '').replace("}", '').replace(" ", ''));
                        xArrayLat.append(str(row[1]).replace("'", '').replace("}", '').replace(" ", ''));
                        yArrayLon.append(str(row[2]).replace("'", '').replace("}", '').replace(" ", ''));
                        print("Added ", "|", row[0], "|", "|", row[1], "|", "|", row[2], "|", "From ", NodeDataFile)

            counttimer = 0;
            while (counttimer < entireSetRepet):
                counttimer = counttimer + 1

                for j in range(0, len(idArray)):

                    xLat = xArrayLat[j]
                    yLon = yArrayLon[j]
                    zMap = '21'
                    driver.get((
                               'https://www.google.com/maps/dir///@' + xLat + ',' + yLon + ',' + zMap + 'z/data=!4m2!4m1!3e0!5m1!1e1'))

                    screenshot = driver.save_screenshot('my_screenshot.png')

                    image = Image.open("my_screenshot.png")

                    foundColor = False
                    holdColor = None
                    holdColorNum = 0
                    holdPixles = None

                    swidth, sheight = image.size
                    Incnum = 100
                    y_Incrarange = int(swidth / 2)
                    x_Incrarange = int(sheight / 2)

                    for b in range(0, 100):
                        k = random.randint((y_Incrarange - Incnum), (y_Incrarange + Incnum))
                        i = random.randint((x_Incrarange - Incnum), (x_Incrarange + Incnum))
                        coordinate = x, y = k, i
                        # print("Point__", i, image.getpixel(coordinate))

                        if ((str(image.getpixel(coordinate)) == '(129, 31, 31, 255)') or (
                            str(image.getpixel(coordinate)) == '(127, 29, 29, 255)')):
                            foundColor = True
                            holdColor = "Dark"
                            holdColorNum = 4
                            holdPixles = str(coordinate)
                            # print("Point__",i,image.getpixel(coordinate), "Dark--------------------------------------------------------------------------------------------------")
                        elif (((str(image.getpixel(coordinate)) == '(242, 60, 50, 255)') or (
                            str(image.getpixel(coordinate)) == '(241, 61, 49, 255)') or (
                            str(image.getpixel(coordinate)) == '(235, 61, 43, 255)')) and holdColor != "Dark"):
                            foundColor = True
                            holdColor = "Red"
                            holdColorNum = 3
                            holdPixles = str(coordinate)
                            # print("Point__",i,image.getpixel(coordinate), "RED---------------------------------------------------------------------------------------------------")
                        elif (((str(image.getpixel(coordinate)) == '(255, 151, 77, 255)') or (
                            str(image.getpixel(coordinate)) == '(255, 151, 76, 255)'))  and holdColor != "Red" and holdColor != "Dark"):
                            foundColor = True
                            holdColor = "Orange"
                            holdColorNum = 2
                            holdPixles = str(coordinate)
                            # print("Point__",i,image.getpixel(coordinate), "Orange---------------------------------------------------------------------------------------------------")
                        elif (((str(image.getpixel(coordinate)) == '(99, 214, 104, 255)') or (
                            str(image.getpixel(coordinate)) == '(99, 213, 104, 255)')) and (
                            holdColor != "Orange") and holdColor != "Red" and holdColor != "Dark"):
                            foundColor = True
                            holdColor = "Green"
                            holdColorNum = 1
                            holdPixles = str(coordinate)
                            # print("Point__",i,image.getpixel(coordinate), "Green---------------------------------------------------------------------------------------------------")

                    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
                    print("ScreenShotTaken___", screenshot, 'at ' + xLat + ',' + yLon)
                    print("TrafficID: ", trafficId)
                    print("nodeID: ", idArray[j])
                    print(j + 1, ' of ', len(idArray))
                    if (foundColor):
                        print("__________", holdColor)
                        print(holdPixles)
                        print(datetime.fromtimestamp(calendar.timegm(time.gmtime())), " Day",
                              datetime.fromtimestamp(calendar.timegm(time.gmtime())).weekday())
                    else:
                        print(datetime.fromtimestamp(calendar.timegm(time.gmtime())), " Day",
                              datetime.fromtimestamp(calendar.timegm(time.gmtime())).weekday())
                        print("__________No Color Found")
                        driver.quit()
                        driver = webdriver.Chrome('/Users/Ares/PycharmProjects/A/webdrivers/chromedriver')

                    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")

                    if (filprint):
                        row = [trafficId, idArray[j], xLat, yLon, holdColorNum, holdPixles,
                               datetime.fromtimestamp(calendar.timegm(time.gmtime())),
                               datetime.fromtimestamp(calendar.timegm(time.gmtime())).weekday()]
                        writer.writerow(row)
                        trafficId = int(trafficId + 1)

        del image
        driver.quit()
        csvFile.close()
        # }


if (__name__ == "__main__"):  # {

    main();


    # }
