import xml.etree.ElementTree as ET

roads = set()
final_roads = []

class Elements:
    def __init__(self, element, type, info):
        self.type = type
        self.element = element
        self.info = info
        self.name = ""
        self.isRoad = False
        self.nodes = set()

    def setStreetName(self, streetname):
        self.name = streetname


    def addNode(self, node):
        self.nodes.add(node)

def getNames(all_elements, highways):
    y = 0
    streets = []
    for elem in all_elements:
        for inf in elem.info:
            if "tag" in inf and "name" in inf and "building" not in inf:
                parts = (inf.split("'v': '", 1))
                if len(parts) > 1:
                    street = parts[1].split("'}", 1)
                    if "tag" not in street[0] and elem.name == "":
                        elem.setStreetName(street[0])
                highways.append(elem)
                y = y+1
    print("The number of roads is " + str(y))


def remove_buildings(element, all_Elements):
    isBuilding = True
    print(element.name)
    for inf in element.info:
        print(inf)
        if "building" in inf:
            print(element.name + " is a building")
            isBuilding = True
        if "addr" in inf:
            print(element.name + " is a building")
            isBuilding = True
        if "phone" in inf:
            print(element.name + " is a building")
            isBuilding = True
    if isBuilding is True:
        all_Elements.remove(element)
    else:
        print(element.name + " is not a building")

def getNodes(all_Elements):
    for elem in all_Elements:
        for inf in elem.info:
            if "nd" in inf and "ref" in inf:
                parts = inf.split("'")
                elem.addNode(parts[3])


def isRoad(element, all_Elements):
    isRoad = False
    for node in element.nodes:
        for elem in all_Elements:
            for elem_node in elem.nodes:
                if node == elem_node:
                    isRoad = True
    if isRoad is True and element.name is not "":
        roads.add(element)


def Remove():
    for x in roads:
        for y in roads:
            if x.name == y.name and x !=y:
                for n in y.nodes:
                    x.nodes.add(n)
                y.nodes.clear()
    for road in roads:
        if road.nodes:
            final_roads.append(road)



def main():
    xmldoc = ET.parse('LilAustin.osm')
    root = xmldoc.getroot()
    all_Elements = []
    highways = []
    for child in root:
        element = child
        tag = child.tag
        attribs = child.attrib
        info = ["INFO"]
        for step_child in child:
            info.append(str(step_child.tag) + " " + str(step_child.attrib))
        new_entry = Elements(element, tag, info)
        all_Elements.append(new_entry)
    getNames(all_Elements, highways)
    getNodes(all_Elements)
    for elem in all_Elements:
        remove_buildings(elem, all_Elements)
    for elem in all_Elements:
        all_Elements.remove(elem)
        isRoad(elem, all_Elements)
        all_Elements.append(elem)
    Remove()
    for road in final_roads:
        print(road.name)
        print(road.nodes)


if __name__ == '__main__':
    main()





